lien de projet : https://minpromessages.firebaseapp.com/
### Présentation ###
L’objectif de ce projet est la réalisation d’une application web de messagerie instantanée en Ecmascript  6 avec Firebase ,HTML5 et Bootstrap.

### Arborescence ###
J’ai utilisé ES6 et SASS Starter Kit pour construire un projet javascript ES6 / SASS exécutable sur n'importe quel navigateur

### Description ###
### Message.js : ### 
C’est une classe contient : 
* Quatre attributs : idKey, objet, corps, status
* Un Constructeur : contient trois paramètre (objet,corps,status)
* Les getters et setters.
### FirebaseImp.js : ###
C’est la classe qui contient les méthodes implémente les services de Firebase

//méthode pour créer un nouveau compte

- creerCompte(email,password)

//méthode permet de connecter un utilisateur par son email et son password

- connexion(email,password)

//permet de cacher la formulaire d'authentification après l'authentification :)

- afterConnexion()

//méthode permet de déconnecter l'utilisateur 

- deconnexion()

//méthode permet d’enregistrer le message dans la BD firebase

- saveMessage(m)

//methode permet de charger tous les messages de la BD firebase

- loadMessages()

### DOMUtile.js ###
Ce fichier permet de manipuler et gérer les éléments de DOM  
### main.js ###
Ce dernier fichier est le fichier qui contient les principaux events listener