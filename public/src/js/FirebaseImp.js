/**
 * Created by Abdelhakim on 15-01-2017.
 */
import {Message} from './Message'
import * as DOM from './DOMUtile'
export class FirebaseImp{

    constructor() {
        // Shortcuts to Firebase SDK features.
        this._database = firebase.database()
        this._auth = firebase.auth()
        this._messagesRef = firebase.database().ref('messages')

    }

    //méthode pour créer un nouveau compte
    creerCompte(email,password)
    {
        this.auth.createUserWithEmailAndPassword(email, password).then(()=>{
            document.querySelector("#error").innerHTML="Votre Compte a été crée avec succès ..."
            document.querySelector("#password").innerHTML="";
        }).catch((error)=> {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            document.querySelector("#error").innerHTML=errorMessage;
        })
    }

    //méthode permet de connecter un utilisateur par son email et son password
    connexion(email,password) {
        this.auth.signInWithEmailAndPassword(email, password).then((user)=>{
            document.querySelector('#authentifiactionForm').classList.add("hide")
            document.querySelector('#pagePrincipal').classList.remove("hide")
            return user
        }).catch((error) =>{
            //S'il y a une erreur d'authentification
            let errorCode = error.code
            let errorMessage = error.message
            document.querySelector("#error").innerHTML=errorMessage
            document.querySelector("#password").innerHTML=""
        })
    }

    //permet de cacher la formulaire d'authentification apres d'authentification :)
    afterConnexion(){
        document.querySelector("#myNavbar").innerHTML=""
        let li = document.createElement("li")
        let a = document.createElement("a")
        a.setAttribute('id','deconnect')
        a.setAttribute('href','#')
        let dec  = document.createTextNode("Déconnexion")
        a.appendChild(dec)
        li.appendChild(a)
        document.querySelector("#myNavbar").appendChild(li)
        document.querySelector("#deconnect").addEventListener("click",e => {
            this.deconnexion()
        })

    }
    //methode permet de deconneter l'utilisateur
    deconnexion() {
        this.auth.signOut().then(() =>{
            document.querySelector('#pagePrincipal').classList.add("hide")
            document.querySelector('#authentifiactionForm').classList.remove("hide")
            document.querySelector("#myNavbar").innerHTML="<li><a href='#'>Bienvenue cher visiteur</a></li>"
            document.querySelector("#error").innerHTML="Deconnexion Avec Succés vous pouvez vous connecter une Autre fois"
        }, (error) =>{
            // An error happened.
        })
    }

    estConnecte(){
        this.auth.onAuthStateChanged((user) =>{
            return user;
        })
    }

    //methode permet de enregistrer le message dans la BD firebase
    saveMessage(m){
        this.messagesRef.push({
            objet : m.objet,
            corps : m.corps,
            status : m.status
        })
    }

    //methode permet de charger tous les messages de la BD firebase
    loadMessages(){
       //  Reference to the /messages/ database path.
        // Make sure we remove all previous listeners.
        this.messagesRef.off();
        // Loads  messages.
        let setMessage = this.messagesRef.on('child_added',data => {
            let val = data.val();
            let message=new Message(val.objet,val.corps,val.status)
             message.idkey=data.key
            DOM.creerLigne(message)
        })
    }

    //methode permet de supprimer le message dans la BD firebase
    deleteMessage(id){
        let adaRef = this.database.ref('messages/'+id)
        adaRef.remove()
            .then(() => {
                console.log("Remove succeeded.")
            })
            .catch((error) => {
                console.log("Remove failed: " + error.message)
            });
    }

    //getters & setters
    get database() {
        return this._database;
    }

    set database(value) {
        this._database = value;
    }

    get messagesRef() {
        return this._messagesRef;
    }

    set messagesRef(value) {
        this._messagesRef = value;
    }

    get auth() {
        return this._auth;
    }

    set auth(value) {
        this._auth = value;
    }
}
