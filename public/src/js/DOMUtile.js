/**
 * Created by Abdelhakim on 18-01-2017.
 */
import {FirebaseImp}  from './FirebaseImp'
import {Message} from './Message'

// Shortcuts to DOM Elements
let objet=document.querySelector('#objet')
let corps=document.querySelector('#message')
let status=document.querySelector('input[name="entree"]')
let tbody=document.querySelector('tbody')

let fb=new FirebaseImp();

//fonction permet d'ajouter un message
export let ajouterLigne =() => {
    let m=new Message(objet.value,corps.value,status.value)
    fb.saveMessage(m)
    objet.value = ''
    corps.value = ''
}

/**
 * Fonction permet de Créer un élément tr (ligne du tableau)
 * @param  object string : L'objet du message à extraire de la zone de texte
 * @param  message string : le message à extraire de la zone de texte
 * @param status string : Normal ou Urgent
 * @return HTMLTableRowElement : Elément de type tr prêt à être inséré dans le tableau.
 */
export let creerLigne = (message) => {

    let rowRef = document.createElement("tr")
    rowRef.setAttribute("id",message.idkey)
    let cell1   = rowRef.insertCell(0)
    let cell2   = rowRef.insertCell(1)
    let cell3   = rowRef.insertCell(2)
    let cell4   = rowRef.insertCell(3)
    let input = document.createElement('input')
    input.setAttribute('name','check')
    input.setAttribute('type','checkbox')
    let input2 = document.createElement('input')
    input2.setAttribute('id','sup')
    input2.setAttribute('value','X')
    input2.setAttribute('type','button')
    input2.setAttribute('class','btn btn-danger')

    //add Event to checkbox
   input.addEventListener('change', CreateSupButton)
   input2.addEventListener('click', supprimerLigne)
    if(status.checked == false){
        rowRef.style.backgroundColor = "#F78181"
    }

    let obj  = document.createTextNode(message.objet)
    let msg  = document.createTextNode(message.corps)

    cell1.appendChild(input)
    cell2.appendChild(obj)
    cell3.appendChild(msg)
    cell4.appendChild(input2)

    tbody.appendChild(rowRef);
    //return rowRef;
}

/**
 * Create Delete Button
 */
export let CreateSupButton = () => {
    if(checked && document.querySelector('#btn_Delete') == null){
        let button = document.createElement('button')
        button.setAttribute('type','button')
        button.setAttribute('id','btn_Delete')
        button.setAttribute('class','btn btn-danger')
        let txt = document.createTextNode('Delete Selected Items')
        button.appendChild(txt)
        document.querySelector('.container').appendChild(button)
        button.addEventListener('click',supprimerLesLignesSelectionnees)
    }else{
        let checkboxesChecked = document.querySelectorAll('input[name="check"]:checked')
        if(checkboxesChecked.length <= 0 && document.querySelector('#btn_Delete') != null){
            let button = document.querySelector('#btn_Delete')
            document.querySelector('.container').removeChild(button)
        }
    }
}

/**
 * Supprime une ligne
 * @param tr HTMLTableRowElement : La ligne qu'on veut supprimer
 */
export let supprimerLigne = (tr) => {
    let My_row = tr.target.parentNode.parentNode
    let id=My_row.id
    fb.deleteMessage(id)
    let tbody = document.querySelector('tbody')
    tbody.removeChild(My_row)
}

/**
 * Supprime toutes les lignes sélectionnées
 */
export let supprimerLesLignesSelectionnees =() =>{
    let checkChecked = document.querySelectorAll('input[name="check"]:checked')
    let tbody = document.querySelector('tbody')
    for (let i = 0; i < checkChecked.length; i++) {
        let tr = checkChecked[i].parentNode.parentNode
        let id=tr.id
        fb.deleteMessage(id)
        tbody.removeChild(tr)
    }
    let button = document.querySelector('#btn_Delete')
    document.querySelector('.container').removeChild(button)
}
