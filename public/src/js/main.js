import * as DOM from './DOMUtile'
import {FirebaseImp} from './FirebaseImp'

//instanciation de l'objet classe FireBaseImp
let fb=new FirebaseImp()
fb.loadMessages()

//Evenement du button Connexion
document.querySelector("#connexion").addEventListener("click",e =>{
    let email = document.querySelector("#email").value
    let password = document.querySelector("#password").value
    currentUser=fb.connexion(email, password)
    fb.afterConnexion()
})

//Evenement du button inscription
document.querySelector("#inscription").addEventListener("click",e => {
    let email = document.querySelector("#email").value;
    let password = document.querySelector("#password").value;
    if(email && password)
        fb.creerCompte(email, password)
    else
        document.querySelector("#email").focus();
})

//Evenement visualiser formulaire de nouveau message
document.querySelector("#ecrireMessage").addEventListener("click",()=>{
    if(currentUser!="visiteur")
    {
        document.querySelector('#formAjout').classList.remove("hide")
    }
})

// Ajouter l'événement click au button
let ajout = document.querySelector('#ajout')
ajout.addEventListener("click",(event) =>{
    DOM.ajouterLigne()
    document.querySelector('#formAjout').classList.add("fadeInUp hide")
})

let annuler = document.querySelector('#annuler')
annuler.addEventListener("click",(event) =>{
    document.querySelector('#formAjout').classList.add("hide")
})

// Ajouter l'événement click au checkbox selectAll
let checkbox = document.querySelector('#selectAll')
checkbox.addEventListener("change",()=> {

    let check = document.querySelectorAll('input[name="check"]')
    for (var i = 0; i < check.length; i++) {
        if (check[i].checked)
            check[i].checked = false
        else
            check[i].checked = true
    }
    let checkChecked = document.querySelectorAll('input[name="check"]:checked');

    if(checkChecked.length <= 0 && document.querySelector('#btn_Delete') != null){
        var button = document.querySelector('#btn_Delete')
        document.querySelector('#delAll').removeChild(button)
    }else if(checkChecked.length > 0 && document.querySelector('#btn_Delete') == null){
        var button = document.createElement('button')
        button.setAttribute('type','button')
        button.setAttribute('id','btn_Delete')
        button.setAttribute('class','btn btn-danger')
        var txt = document.createTextNode('Supprimer tous')
        button.appendChild(txt)
        console.log(button)
        document.querySelector('#delAll').appendChild(button)
        button.addEventListener('click',DOM.supprimerLesLignesSelectionnees)
    }
})

let currentUser ="visiteur"