/**
 * Created by Abdelhakim on 11-01-2017.
 */
export class Message {

    constructor (objet,corps,status){
        this._idkey=null
        this._objet=objet
        this._corps=corps
        this._status=status
    }

    get idkey() {
        return this._idkey;
    }

    set idkey(val){
        this._idkey=val;
    }

    get objet() {
        return this._objet;
    }

    set objet(value) {
        this._objet = value;
    }

    get corps() {
        return this._corps;
    }

    set corps(value) {
        this._corps = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }
}