(function (global, factory) {
    if (typeof define === "function" && define.amd) {
        define('main', [], factory);
    } else if (typeof exports !== "undefined") {
        factory();
    } else {
        var mod = {
            exports: {}
        };
        factory();
        global.main = mod.exports;
    }
})(this, function () {
    'use strict';

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];
                descriptor.enumerable = descriptor.enumerable || false;
                descriptor.configurable = true;
                if ("value" in descriptor) descriptor.writable = true;
                Object.defineProperty(target, descriptor.key, descriptor);
            }
        }

        return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);
            if (staticProps) defineProperties(Constructor, staticProps);
            return Constructor;
        };
    }();

    var Message = function () {
        function Message(objet, corps, status) {
            _classCallCheck(this, Message);

            this._idkey = null;
            this._objet = objet;
            this._corps = corps;
            this._status = status;
        }

        _createClass(Message, [{
            key: 'idkey',
            get: function get() {
                return this._idkey;
            },
            set: function set(val) {
                this._idkey = val;
            }
        }, {
            key: 'objet',
            get: function get() {
                return this._objet;
            },
            set: function set(value) {
                this._objet = value;
            }
        }, {
            key: 'corps',
            get: function get() {
                return this._corps;
            },
            set: function set(value) {
                this._corps = value;
            }
        }, {
            key: 'status',
            get: function get() {
                return this._status;
            },
            set: function set(value) {
                this._status = value;
            }
        }]);

        return Message;
    }();

    var FirebaseImp = function () {
        function FirebaseImp() {
            _classCallCheck(this, FirebaseImp);

            this._database = firebase.database();
            this._auth = firebase.auth();
            this._messagesRef = firebase.database().ref('messages');
        }

        _createClass(FirebaseImp, [{
            key: 'creerCompte',
            value: function creerCompte(email, password) {
                this.auth.createUserWithEmailAndPassword(email, password).then(function () {
                    document.querySelector("#error").innerHTML = "Votre Compte a été crée avec succès ...";
                    document.querySelector("#password").innerHTML = "";
                }).catch(function (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    document.querySelector("#error").innerHTML = errorMessage;
                });
            }
        }, {
            key: 'connexion',
            value: function connexion(email, password) {
                this.auth.signInWithEmailAndPassword(email, password).then(function (user) {
                    document.querySelector('#authentifiactionForm').classList.add("hide");
                    document.querySelector('#pagePrincipal').classList.remove("hide");
                    return user;
                }).catch(function (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    document.querySelector("#error").innerHTML = errorMessage;
                    document.querySelector("#password").innerHTML = "";
                });
            }
        }, {
            key: 'afterConnexion',
            value: function afterConnexion() {
                var _this = this;

                document.querySelector("#myNavbar").innerHTML = "";
                var li = document.createElement("li");
                var a = document.createElement("a");
                a.setAttribute('id', 'deconnect');
                a.setAttribute('href', '#');
                var dec = document.createTextNode("Déconnexion");
                a.appendChild(dec);
                li.appendChild(a);
                document.querySelector("#myNavbar").appendChild(li);
                document.querySelector("#deconnect").addEventListener("click", function (e) {
                    _this.deconnexion();
                });
            }
        }, {
            key: 'deconnexion',
            value: function deconnexion() {
                this.auth.signOut().then(function () {
                    document.querySelector('#pagePrincipal').classList.add("hide");
                    document.querySelector('#authentifiactionForm').classList.remove("hide");
                    document.querySelector("#myNavbar").innerHTML = "<li><a href='#'>Bienvenue cher visiteur</a></li>";
                    document.querySelector("#error").innerHTML = "Deconnexion Avec Succés vous pouvez vous connecter une Autre fois";
                }, function (error) {});
            }
        }, {
            key: 'estConnecte',
            value: function estConnecte() {
                this.auth.onAuthStateChanged(function (user) {
                    return user;
                });
            }
        }, {
            key: 'saveMessage',
            value: function saveMessage(m) {
                this.messagesRef.push({
                    objet: m.objet,
                    corps: m.corps,
                    status: m.status
                });
            }
        }, {
            key: 'loadMessages',
            value: function loadMessages() {
                this.messagesRef.off();

                var setMessage = this.messagesRef.on('child_added', function (data) {
                    var val = data.val();
                    var message = new Message(val.objet, val.corps, val.status);
                    message.idkey = data.key;
                    creerLigne(message);
                });
            }
        }, {
            key: 'deleteMessage',
            value: function deleteMessage(id) {
                var adaRef = this.database.ref('messages/' + id);
                adaRef.remove().then(function () {
                    console.log("Remove succeeded.");
                }).catch(function (error) {
                    console.log("Remove failed: " + error.message);
                });
            }
        }, {
            key: 'database',
            get: function get() {
                return this._database;
            },
            set: function set(value) {
                this._database = value;
            }
        }, {
            key: 'messagesRef',
            get: function get() {
                return this._messagesRef;
            },
            set: function set(value) {
                this._messagesRef = value;
            }
        }, {
            key: 'auth',
            get: function get() {
                return this._auth;
            },
            set: function set(value) {
                this._auth = value;
            }
        }]);

        return FirebaseImp;
    }();

    var objet = document.querySelector('#objet');
    var corps = document.querySelector('#message');
    var status = document.querySelector('input[name="entree"]');
    var tbody = document.querySelector('tbody');

    var fb$1 = new FirebaseImp();

    var ajouterLigne = function ajouterLigne() {
        var m = new Message(objet.value, corps.value, status.value);
        fb$1.saveMessage(m);
        objet.value = '';
        corps.value = '';
    };

    var creerLigne = function creerLigne(message) {

        var rowRef = document.createElement("tr");
        rowRef.setAttribute("id", message.idkey);
        var cell1 = rowRef.insertCell(0);
        var cell2 = rowRef.insertCell(1);
        var cell3 = rowRef.insertCell(2);
        var cell4 = rowRef.insertCell(3);
        var input = document.createElement('input');
        input.setAttribute('name', 'check');
        input.setAttribute('type', 'checkbox');
        var input2 = document.createElement('input');
        input2.setAttribute('id', 'sup');
        input2.setAttribute('value', 'X');
        input2.setAttribute('type', 'button');
        input2.setAttribute('class', 'btn btn-danger');

        input.addEventListener('change', CreateSupButton);
        input2.addEventListener('click', supprimerLigne);
        if (status.checked == false) {
            rowRef.style.backgroundColor = "#F78181";
        }

        var obj = document.createTextNode(message.objet);
        var msg = document.createTextNode(message.corps);

        cell1.appendChild(input);
        cell2.appendChild(obj);
        cell3.appendChild(msg);
        cell4.appendChild(input2);

        tbody.appendChild(rowRef);
    };

    var CreateSupButton = function CreateSupButton() {
        if (checked && document.querySelector('#btn_Delete') == null) {
            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('id', 'btn_Delete');
            button.setAttribute('class', 'btn btn-danger');
            var txt = document.createTextNode('Delete Selected Items');
            button.appendChild(txt);
            document.querySelector('.container').appendChild(button);
            button.addEventListener('click', supprimerLesLignesSelectionnees);
        } else {
            var checkboxesChecked = document.querySelectorAll('input[name="check"]:checked');
            if (checkboxesChecked.length <= 0 && document.querySelector('#btn_Delete') != null) {
                var _button = document.querySelector('#btn_Delete');
                document.querySelector('.container').removeChild(_button);
            }
        }
    };

    var supprimerLigne = function supprimerLigne(tr) {
        var My_row = tr.target.parentNode.parentNode;
        var id = My_row.id;
        fb$1.deleteMessage(id);
        var tbody = document.querySelector('tbody');
        tbody.removeChild(My_row);
    };

    var supprimerLesLignesSelectionnees = function supprimerLesLignesSelectionnees() {
        var checkChecked = document.querySelectorAll('input[name="check"]:checked');
        var tbody = document.querySelector('tbody');
        for (var i = 0; i < checkChecked.length; i++) {
            var tr = checkChecked[i].parentNode.parentNode;
            var id = tr.id;
            fb$1.deleteMessage(id);
            tbody.removeChild(tr);
        }
        var button = document.querySelector('#btn_Delete');
        document.querySelector('.container').removeChild(button);
    };

    var fb = new FirebaseImp();
    fb.loadMessages();

    document.querySelector("#connexion").addEventListener("click", function (e) {
        var email = document.querySelector("#email").value;
        var password = document.querySelector("#password").value;
        currentUser = fb.connexion(email, password);
        fb.afterConnexion();
    });

    document.querySelector("#inscription").addEventListener("click", function (e) {
        var email = document.querySelector("#email").value;
        var password = document.querySelector("#password").value;
        if (email && password) fb.creerCompte(email, password);else document.querySelector("#email").focus();
    });

    document.querySelector("#ecrireMessage").addEventListener("click", function () {
        if (currentUser != "visiteur") {
            document.querySelector('#formAjout').classList.remove("hide");
        }
    });

    var ajout = document.querySelector('#ajout');
    ajout.addEventListener("click", function (event) {
        ajouterLigne();
        document.querySelector('#formAjout').classList.add("hide");
    });

    var annuler = document.querySelector('#annuler');
    annuler.addEventListener("click", function (event) {
        document.querySelector('#formAjout').classList.add("hide");
    });

    var checkbox = document.querySelector('#selectAll');
    checkbox.addEventListener("change", function () {

        var check = document.querySelectorAll('input[name="check"]');
        for (var i = 0; i < check.length; i++) {
            if (check[i].checked) check[i].checked = false;else check[i].checked = true;
        }
        var checkChecked = document.querySelectorAll('input[name="check"]:checked');

        if (checkChecked.length <= 0 && document.querySelector('#btn_Delete') != null) {
            var button = document.querySelector('#btn_Delete');
            document.querySelector('#delAll').removeChild(button);
        } else if (checkChecked.length > 0 && document.querySelector('#btn_Delete') == null) {
            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('id', 'btn_Delete');
            button.setAttribute('class', 'btn btn-danger');
            var txt = document.createTextNode('Supprimer tous');
            button.appendChild(txt);
            console.log(button);
            document.querySelector('#delAll').appendChild(button);
            button.addEventListener('click', supprimerLesLignesSelectionnees);
        }
    });

    var currentUser = "visiteur";
});